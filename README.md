# WeSplit

WeSplit is a check-splitting app allows the user enter the cost of his food, select how much of a tip he wants to leave, and how many people he is with, and it will tell him how much each person needs to pay.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/wesplit-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Form
- NavigationStack
- \@State
- Picker